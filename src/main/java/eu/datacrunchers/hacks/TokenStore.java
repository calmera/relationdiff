package eu.datacrunchers.hacks;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import java.util.HashMap;
import java.util.Map;

public class TokenStore {
    private Map<API, Token> tokens = new HashMap<API, Token>();

    public Token connectDirect(API api, String token, String secret) {
        this.tokens.put(api, new Token(token, secret));
        
        return this.tokens.get(api);
    }

    public Token connect(API api, String key, String secret, AuthorizationCallback callback) {
        if (! tokens.containsKey(api)) {
            OAuthService service = new ServiceBuilder()
                    .provider(api.getApiClass())
                    .apiKey(key)
                    .apiSecret(secret)
                    .build();

            Token requestToken = service.getRequestToken();

            String authUrl = service.getAuthorizationUrl(requestToken);

            Verifier verifier = new Verifier(callback.verify(authUrl));

            Token accessToken = service.getAccessToken(requestToken, verifier); // the requestToken you had from step 2

            System.out.println("Received token '" + accessToken.getToken() + "' and secret '" + accessToken.getSecret() + "'");

            this.tokens.put(api, accessToken);
        }

        return this.tokens.get(api);
    }


}
