package eu.datacrunchers.hacks;

import org.scribe.builder.api.Api;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.builder.api.TwitterApi;

public enum API {
    Twitter(TwitterApi.class),
    Linkedin(LinkedInApi.class);
    
    private Class<? extends Api> apiClass;
    private API(Class<? extends Api> apiClass) {
        this.apiClass = apiClass;
    }

    public Class<? extends Api> getApiClass() {
        return apiClass;
    }
}
