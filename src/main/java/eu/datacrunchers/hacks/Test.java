package eu.datacrunchers.hacks;

import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.schema.Connections;
import com.google.code.linkedinapi.schema.Person;
import org.apache.commons.cli.*;
import org.scribe.model.Token;
import twitter4j.*;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Test implements AuthorizationCallback {
    private TokenStore tokenStore = new TokenStore();

    public void launch(String[] args) throws TwitterException, IOException, ParseException {
        Options options = new Options();
        options.addOption("l", false, "Connect to linkedIn");
        options.addOption("t", false, "Connect to twitter");
        options.addOption("c", true, "The consumer key");
        options.addOption("C", true, "The consumer secret");
        options.addOption("a", true, "The Access Token");
        options.addOption("A", true, "The Access Token Secret");
        options.addOption("u", true, "The user for which to retrieve the information");

        CommandLineParser commandLineParser = new PosixParser();

        CommandLine cli = commandLineParser.parse(options, args);
        
        Token token = null;
        if (cli.hasOption('a') && cli.hasOption('A')) {
            token = new Token(
                    cli.getOptionValue('a'),
                    cli.getOptionValue('A')
            );
        }
        
        String key = cli.getOptionValue('c');
        String secret = cli.getOptionValue('C');
        String user = cli.getOptionValue('u');

        if (cli.hasOption('l')) {
            LinkedInApiClient linkedIn;
            if (token == null) {
                linkedIn = connectLinkedIn(key, secret);
            } else {
                linkedIn = connectLinkedIn(key, secret, token);
            }

            Connections connections = linkedIn.getConnectionsById(user);
            for (Person person : connections.getPersonList()) {
                System.out.println("Follower: " + person);
            }

        } else if (cli.hasOption('t')) {
            Twitter twitter;
            if (token == null) {
                twitter = connectTwitter(key, secret);
            } else {
                twitter = connectTwitter(key, secret, token);
            }
            
            User usr = twitter.showUser(user);
            System.out.println("User: " + usr);

            IDs ids = twitter.getFollowersIDs(usr.getId(), -1);
            StringBuilder builder = new StringBuilder();
            boolean first = true;
            for (long id : ids.getIDs()) {
                if (! first) builder.append(", ");

                builder.append(id);

                first = false;
            }
            
            System.out.println("Followers: " + builder.toString());
        }
    }

    protected Twitter connectTwitter(String consumerKey, String consumerSecret) throws IOException, TwitterException {
        Token token = tokenStore.connect(API.Twitter, consumerKey, consumerSecret, this);

        return connectTwitter(consumerKey, consumerSecret, token);
    }

    protected Twitter connectTwitter(String consumerKey, String consumerSecret, Token token) throws IOException, TwitterException {
        Configuration configuration = new ConfigurationBuilder()
                .setOAuthConsumerKey(consumerKey)
                .setOAuthConsumerSecret(consumerSecret)
                .setOAuthAccessToken(token.getToken())
                .setOAuthAccessTokenSecret(token.getSecret())
                .build();

        return new TwitterFactory(configuration).getInstance();
    }

    protected LinkedInApiClient connectLinkedIn(String consumerKey, String consumerSecret) throws IOException, TwitterException {
        Token token = tokenStore.connect(API.Linkedin, consumerKey, consumerSecret, this);

        return connectLinkedIn(consumerKey, consumerSecret, token);
    }

    protected LinkedInApiClient connectLinkedIn(String consumerKey, String consumerSecret, Token token) throws IOException, TwitterException {
        LinkedInApiClientFactory factory = LinkedInApiClientFactory.newInstance(consumerKey, consumerSecret);
        return factory.createLinkedInApiClient(token.getToken(), token.getSecret());
    }

    public String verify(String url) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            System.out.println("Open the following URL and grant access to your account:");
            System.out.println(url);
            System.out.print("Enter the Verifier (if available) or just hit enter.[PIN]:");

            return br.readLine();

        } catch (IOException ioe) {
            ioe.printStackTrace();
            
            return null;
        }
    }

    public static void main(String[] args) {
        try {
            new Test().launch(args);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }
}
