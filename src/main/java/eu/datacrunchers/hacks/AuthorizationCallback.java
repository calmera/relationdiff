package eu.datacrunchers.hacks;

public interface AuthorizationCallback {
    
    String verify(String url);
}
